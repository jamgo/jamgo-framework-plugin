#!/bin/bash

FRAMEWORK_VERSION="8.0.2"
PLUGIN_VERSION="1.0.0"
APP_NAME=""
APP_PACKAGE=""
DB_PASSWORD=""
DB_NAME=""
DB_SCHEMA=""
DB_USER=""
DOCKER_REPOSITORY=""
GROUP_ID=""
ORGANIZATION_NAME=""
OVERWRITE=""
SNAPSHOT=""
THEME_NAME=""
GENERATE_BACKOFFICE_APP="true"
GENERATE_WEB_API_APP="true"

function usage() {
    printf "\n"
    printf "Create jamgo framework skeleton app\n"
    printf "\n"
    printf "Usage:\n"
    printf "\tcreateSketeton.sh [<appName>] [options]\n"
    printf "\tcreateSketeton.sh -h|--help\n"
    printf "\n"
    printf "\tappName\tName of the generated application (default 'demo')\n"
    printf "\n"
    printf "Options:\n"
    printf "\t--app-description <appDescription>            Short application description (default: <appName>)\n"
    printf "\t--db-name <dbName>                            Database name (default: <appName>)\n"
    printf "\t--db-password <dbPassword>                    Database password (default: <organizationName>)\n"
    printf "\t--db-schema <dbSchema>                        Database schema (default: <*appNameLowerCase>)\n"
    printf "\t--db-user <dbUser>                            Database user (default: <organizationName>)\n"
    printf "\t--docker-repository <dockerRepository>        Docker repository name for image generation scripts (default: organizationName)\n"
    printf "\t-f,--framework-version <framework_version>    Jamgo framework version to replace default in init.gradle\n"
    printf "\t--group-id <groupId>                          Artifacts group id (default: org.jamgo)\n"
    printf "\t--organization-name <organizationName>        Organization name (default: <last part of groupId>)\n"
    printf "\t--overwrite                                   Overwrites existing files\n"
    printf "\t--no-backoffice                               Does not generate backoffice app (default: generate)\n"
    printf "\t--no-web-api                                  Does not generate web api app (default: generate)\n"
    printf "\t--app-package <appPackage>                    Base package name of generated classes (default: <groupId>.<*appNameLowerCase>)\n"
    printf "\t-p,--plugin-version <plugin_version>          Jamgo framework plugin version to replace default in init.gradle\n"
    printf "\t-s,--snapshot                                 Add maven central snapshots repository (set if using any SNAPSHOT version)\n"
    printf "\t--theme-name <themeName>                      Theme name for vaadin (default: <appName>)\n"
    printf "\n"
    printf "\t*appNameLowerCase: appName lower case without word separators. Ie: foo-bar -> foobar\n"
    printf "\n"
}

OPTS=$(getopt -o hf:p:s --long help,app-description:,db-name:,db-password:,db-schema:,db-user:,docker-repository:,framework-version:,group-id:,organization-name:,overwrite,no-backoffice,no-web-api,app-package:,plugin-version:,snapshot,theme-name: -- "$@")
if [[ $? -ne 0 ]]; then
    exit 1;
fi

eval set -- "$OPTS"
while (($#)) 
do
	case "$1" in
		-h | --help)
			usage
			exit
			;;
		-pv | --plugin-version)
			PLUGIN_VERSION=$2;
			shift
			;;
		--app-name)
			APP_NAME=$2
			shift
			;;
		--app-package)
			APP_PACKAGE=$2
			shift
			;;
		--db-password)
			DB_PASSWORD=$2
			shift
			;;
		--db-name)
			DB_NAME=$2
			shift
			;;
		--db-schema)
			DB_SCHEMA=$2
			shift
			;;
		--db-user)
			DB_USER=$2
			shift
			;;
		--docker-repository)
			DOCKER_REPOSITORY=$2
			shift
			;;
		-f | --framework-version)
			FRAMEWORK_VERSION=$2
			shift
			;;
		--group-id)
			GROUP_ID=$2
			shift
			;;
		--organization-name)
			ORGANIZATION_NAME=$2
			shift
			;;
		--overwrite)
			OVERWRITE="true"
			;;
		--no-backoffice)
			GENERATE_BACKOFFICE_APP="false"
			;;
		--no-web-api)
			GENERATE_WEB_API_APP="false"
			;;
		-p | --plugin-version)
			PLUGIN_VERSION=$2
			shift
			;;
		-s | --snapshot)
			SNAPSHOT="true"
			;;
		--theme-name)
			THEME_NAME=$2
			shift 
			;;
		--)
			shift
			break
			;;
	esac
	shift
done

APP_NAME=$1

cat > init.gradle << EOF
initscript {
	repositories {
		mavenLocal()
		mavenCentral()		
EOF

if [ -n "$SNAPSHOT" ]; then
	echo "		maven { url 'https://oss.sonatype.org/content/repositories/snapshots' }" >> init.gradle
fi
		
cat >> init.gradle << EOF
	}
	dependencies {
EOF

if [ -n "$PLUGIN_VERSION" ]; then
	echo "		classpath('org.jamgo:jamgo-framework-plugin:$PLUGIN_VERSION')" >> init.gradle
fi

cat >> init.gradle << EOF
	}
}

rootProject {
	apply plugin: 'java'
	apply plugin: org.jamgo.framework.JamgoFrameworkPlugin

	createAppSkeleton {
EOF

if [ -n "$APP_NAME" ]; then
	echo "		appName \"$APP_NAME\"" >> init.gradle
fi
if [ -n "$DB_PASSWORD" ]; then
	echo "		dbPassword \"$DB_PASSWORD\"" >> init.gradle
fi
if [ -n "$DB_NAME" ]; then
	echo "		dbName \"$DB_NAME\"" >> init.gradle
fi
if [ -n "$DB_SCHEMA" ]; then
	echo "		dbSchema \"$DB_SCHEMA\"" >> init.gradle
fi
if [ -n "$DB_USER" ]; then
	echo "		dbUser \"$DB_USER\"" >> init.gradle
fi
if [ -n "$DOCKER_REPOSITORY" ]; then
	echo "		dockerRepository \"$DOCKER_REPOSITORY\"" >> init.gradle
fi
if [ -n "$FRAMEWORK_VERSION" ]; then
	echo "		frameworkVersion \"$FRAMEWORK_VERSION\"" >> init.gradle
fi
if [ -n "$GROUP_ID" ]; then
	echo "		groupId \"$GROUP_ID\"" >> init.gradle
fi
if [ -n "$ORGANIZATION_NAME" ]; then
	echo "		organizationName \"$ORGANIZATION_NAME\"" >> init.gradle
fi
if [ -n "$OVERWRITE" ]; then
	echo "		overwrite true" >> init.gradle
fi
if [ -n "$APP_PACKAGE" ]; then
	echo "		appPackage \"$APP_PACKAGE\"" >> init.gradle
fi
if [ -n "$THEME_NAME" ]; then
	echo "		themeName \"$THEME_NAME\"" >> init.gradle
fi
echo "		generateBackofficeApp $GENERATE_BACKOFFICE_APP" >> init.gradle
echo "		generateWebApiApp $GENERATE_WEB_API_APP" >> init.gradle

cat >> init.gradle << EOF
	}
}
EOF

touch settings.gradle
gradle wrapper --gradle-version 7.6
./gradlew -I init.gradle createAppSkeleton
git init
git add .
git commit -m "init: initial commit"
