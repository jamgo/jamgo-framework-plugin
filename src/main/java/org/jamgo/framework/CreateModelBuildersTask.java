package org.jamgo.framework;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.gradle.api.DefaultTask;
import org.gradle.api.logging.Logger;
import org.gradle.api.logging.Logging;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.TaskAction;
import org.gradle.api.tasks.compile.JavaCompile;
import org.reflections.Reflections;
import org.reflections.scanners.Scanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import com.google.common.base.Charsets;
import com.google.common.io.CharSink;
import com.google.common.io.Files;

public class CreateModelBuildersTask extends DefaultTask {

	private final static String DEFAULT_LOG_LEVEL = "ERROR";
	private final static String DEFAULT_OUTPUT_DIRECTORY = "src/testFixtures/java";

	private final Logger logger = Logging.getLogger(this.getClass());

	@Input
	private String baseModelClass;
	@Input
	private String baseModelBuilderPackage;
	@Input
	private String[] basePackages;

	public String getBaseModelClass() {
		return this.baseModelClass;
	}

	public void setBaseModelClass(final String baseModelClass) {
		this.baseModelClass = baseModelClass;
	}

	public String getBaseModelBuilderPackage() {
		return this.baseModelBuilderPackage;
	}

	public void setBaseModelBuilderPackage(final String baseModelBuilderPackage) {
		this.baseModelBuilderPackage = baseModelBuilderPackage;
	}

	public String[] getBasePackages() {
		return this.basePackages;
	}

	public void setBasePackages(final String[] basePackages) {
		this.basePackages = basePackages;
	}

	@TaskAction
	void performCreateModelBuildersTask() throws ClassNotFoundException, IOException {
		if (ArrayUtils.isEmpty(this.basePackages)) {
			return;
		}

		System.out.println(this.basePackages[0]);

		URL[] classPath = null;
		classPath = this.getClassPath();
		this.logger.info("ClassPath: " + Arrays.toString(classPath));
		final URLClassLoader classLoader = new URLClassLoader(classPath, Thread.currentThread().getContextClassLoader());

		final ModelBuilderFactory modelBuilderFactory = new ModelBuilderFactory(
			classLoader.loadClass(this.baseModelClass),
			this.baseModelBuilderPackage);

		final Reflections reflections = new Reflections(
			(new ConfigurationBuilder())
				.addClassLoader(classLoader)
				.setScanners(new Scanner[] { new SubTypesScanner(), new TypeAnnotationsScanner() })
				.setUrls(ClasspathHelper.forPackage(this.basePackages[0], classLoader))
				.filterInputsBy((new FilterBuilder()).includePackage(this.basePackages[0])));

		final Set<Class<?>> entityClasses = reflections.getTypesAnnotatedWith(Entity.class);

		entityClasses.forEach(eachEntityClass -> {
			final String sourceFilePath = new StringBuilder()
				.append(this.getProject().getProjectDir().getAbsolutePath())
				.append(File.separator)
				.append(CreateModelBuildersTask.DEFAULT_OUTPUT_DIRECTORY)
				.append(File.separator)
				.append((eachEntityClass.getPackage().getName() + ModelBuilderFactory.BUILDER_PACKAGE_SUFFIX).replace(".", File.separator))
				.toString();
			final String sourceFileName = new StringBuilder()
				.append(sourceFilePath)
				.append(File.separator)
				.append(StringUtils.capitalize(eachEntityClass.getSimpleName()))
				.append(ModelBuilderFactory.BUILDER_CLASS_SUFFIX).append(".java")
				.toString();

			final File sourceFile = new File(sourceFileName);

			if (!sourceFile.exists()) {
				try {
					this.createDirectories(sourceFilePath);
					final String modelBuilderCode = modelBuilderFactory.createBuilderSource(eachEntityClass.getTypeName(), classLoader);
					final CharSink sink = Files.asCharSink(sourceFile, Charsets.UTF_8);
					sink.write(modelBuilderCode);
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}

	private URI getCompiledClassesDir() {
		return this.getProject().getTasks().withType(JavaCompile.class).getByName("compileJava").getDestinationDir().toURI();
	}

	private URL[] getClassPath() throws MalformedURLException {
		final List<URL> urls = new ArrayList<>();
		urls.add(this.getCompiledClassesDir().toURL());
		for (final File eachFile : this.getProject().getConfigurations().findByName("compileClasspath").getFiles()) {
			urls.add(eachFile.toURI().toURL());
		}
		return urls.toArray(new URL[urls.size()]);
	}

	private void createDirectories(final String pathString) throws IOException {
		final Path path = Paths.get(pathString);
		java.nio.file.Files.createDirectories(path);
	}

}
