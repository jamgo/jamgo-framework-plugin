package org.jamgo.framework;

public class JamgoFrameworkPluginExtension {

	private String entitiesPackage = "org.jamgo.model.entity";

	public String getEntitiesPackage() {
		return this.entitiesPackage;
	}

	public void setEntitiesPackage(final String entitiesPackage) {
		this.entitiesPackage = entitiesPackage;
	}

}
