package org.jamgo.framework;

import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.lang.model.element.Modifier;

import org.apache.commons.lang3.StringUtils;
import org.reflections.ReflectionUtils;
import org.springframework.stereotype.Repository;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

public class ModelRepositoryFactory {

	public final static String REPOSITORY_SUPERCLASS_NAME = "ModelRepository";
	public final static String REPOSITORY_CLASS_SUFFIX = "Repository";
	public final static String REPOSITORY_PACKAGE_SUFFIX = ".repository";

	private final static Map<Class<?>, Class<?>> PRIMITIVE_CLASSES = new HashMap<>();
	static {
		ModelRepositoryFactory.PRIMITIVE_CLASSES.put(int.class, Integer.class);
		ModelRepositoryFactory.PRIMITIVE_CLASSES.put(long.class, Long.class);
		ModelRepositoryFactory.PRIMITIVE_CLASSES.put(short.class, Short.class);
		ModelRepositoryFactory.PRIMITIVE_CLASSES.put(byte.class, Byte.class);
		ModelRepositoryFactory.PRIMITIVE_CLASSES.put(float.class, Float.class);
		ModelRepositoryFactory.PRIMITIVE_CLASSES.put(double.class, Double.class);
		ModelRepositoryFactory.PRIMITIVE_CLASSES.put(char.class, Character.class);
		ModelRepositoryFactory.PRIMITIVE_CLASSES.put(boolean.class, Boolean.class);
	}

	private final Class<?> baseRepositoryClass;

	public ModelRepositoryFactory(final Class<?> baseRepositoryClass) {
		super();
		this.baseRepositoryClass = baseRepositoryClass;
	}

	@SuppressWarnings("unchecked")
	public String createRepositorySource(final String modelClassName, final URLClassLoader classLoader) {
		final Class<?> modelClass = ReflectionUtils.forName(modelClassName, classLoader);
		final ClassName repositoryClassName = ClassName.get(getRepositoryPackageName(modelClass), getRepositoryClassName(modelClass));

		final List<Field> instanceFields = new ArrayList<>();
		for (final Field eachField : ReflectionUtils.getAllFields(modelClass)) {
			if (!java.lang.reflect.Modifier.isStatic(eachField.getModifiers())
				&& eachField.getName() != "id" && eachField.getName() != "version") {

				instanceFields.add(eachField);
			}
		}

		final List<MethodSpec> repositoryMethods = new ArrayList<>();

		this.addGetModelClass(repositoryMethods, modelClass);

		final TypeName modelClassTypeName = TypeName.get(modelClass);
		final TypeName superclassTypeName = ParameterizedTypeName.get(ClassName.get(this.baseRepositoryClass), modelClassTypeName);

		final TypeSpec builderSpec = TypeSpec.classBuilder(repositoryClassName)
			.addModifiers(Modifier.PUBLIC)
			.addAnnotation(Repository.class)
			.superclass(superclassTypeName)
			.addMethods(repositoryMethods)
			.build();

		final JavaFile builderJavaFile = JavaFile.builder(repositoryClassName.packageName(), builderSpec)
			.indent("\t")
			.skipJavaLangImports(true)
			.build();

		final StringWriter writer = new StringWriter();
		try {
			builderJavaFile.writeTo(writer);
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return writer.toString();
	}

	private void addGetModelClass(final List<MethodSpec> methodSpecs, final Class<?> modelClass) {
		final TypeName returnTypeName = ParameterizedTypeName.get(Class.class, modelClass);

		final MethodSpec.Builder getModelClassSpecBuilder = MethodSpec.methodBuilder("getModelClass")
			.addAnnotation(Override.class)
			.addModifiers(Modifier.PROTECTED)
			.returns(returnTypeName);

		this.addReturnStatement(getModelClassSpecBuilder, modelClass);

		methodSpecs.add(getModelClassSpecBuilder.build());
	}

	private void addReturnStatement(final MethodSpec.Builder methodBuilder, final Class<?> modelClass) {
		final StringBuilder statementBuilder = new StringBuilder();
		statementBuilder
			.append("return ").append(modelClass.getSimpleName()).append(".class");

		methodBuilder.addStatement(statementBuilder.toString());
	}

	public static String getRepositoryClassName(final Class<?> modelClass) {
		return modelClass.getSimpleName() + ModelRepositoryFactory.REPOSITORY_CLASS_SUFFIX;
	}

	public static String getRepositoryPackageName(final Class<?> modelClass) {
		final String modelClassPackageName = modelClass.getPackage().getName();
		final String basePackageName = StringUtils.left(modelClassPackageName, StringUtils.lastIndexOf(modelClassPackageName, "."));
		return basePackageName + ModelRepositoryFactory.REPOSITORY_PACKAGE_SUFFIX;
	}

}
