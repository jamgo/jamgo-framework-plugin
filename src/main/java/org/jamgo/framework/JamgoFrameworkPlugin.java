package org.jamgo.framework;

import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.Task;

public class JamgoFrameworkPlugin implements Plugin<Project> {

	@Override
	public void apply(final Project project) {
		final Task compileJavaTask = project.getTasks().findByName("compileJava");

		final Task createAppSkeletonTask = project.getTasks().create("createAppSkeleton", CreateAppSkeletonTask.class);
		createAppSkeletonTask.dependsOn(compileJavaTask);
		final Task createModelBuildersTask = project.getTasks().create("createModelBuilders", CreateModelBuildersTask.class);
		createModelBuildersTask.dependsOn(compileJavaTask);
		final Task createModelRepositoriesTask = project.getTasks().create("createModelRepositories", CreateModelRepositoriesTask.class);
		createModelRepositoriesTask.dependsOn(compileJavaTask);

//		final JamgoFrameworkPluginExtension extension = project.getExtensions().create("jamgoFramework", JamgoFrameworkPluginExtension.class);
//		project.getTasks().register("hello", task -> {
//			task.doLast(s -> System.out.println("entitiesPackage: " + extension.getEntitiesPackage()));
//		});
	}

}
