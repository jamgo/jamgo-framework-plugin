package org.jamgo.framework;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.CaseUtils;
import org.gradle.api.DefaultTask;
import org.gradle.api.logging.Logger;
import org.gradle.api.logging.Logging;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.TaskAction;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import com.google.common.base.Charsets;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.io.ByteSource;
import com.google.common.io.Files;
import com.google.common.io.Resources;
import com.hubspot.jinjava.Jinjava;

public class CreateAppSkeletonTask extends DefaultTask {

	private final static String TEMPLATES_PACKAGE = "templates";
	private final static String TEMPLATES_PATH_COMMON = "templates/common/";
	private final static String TEMPLATES_PATH_WEB_API = "templates/web-api/";
	private final static String TEMPLATES_PATH_BACKOFFICE = "templates/backoffice/";
	private final static String TEMPLATES_PATH_FULL = "templates/full/";
	private final static String TEMPLATE_SUFFIX = ".template";
	private final static String PROPERTIES_TEMPLATE_SUFFIX = ".properties" + CreateAppSkeletonTask.TEMPLATE_SUFFIX;
	private final static String APPLICATION_CLASS_SUFFIX = "BackofficeApplication";

	private final Logger logger = Logging.getLogger(this.getClass());

	@Input
	private String groupId;
	@Input
	private String organizationName;
	@Input
	private String appName;
	@Input
	private String themeName;
	@Input
	private String appDescription;
	@Input
	private String dbName;
	@Input
	private String dbSchema;
	@Input
	private String dbUser;
	@Input
	private String dbPassword;
	@Input
	private String appPackage;
	@Input
	private String frameworkVersion;
	@Input
	private String dockerRepository;
	@Input
	private boolean overwrite = false;
	@Input
	private boolean generateBackofficeApp = true;
	@Input
	private boolean generateWebApiApp = true;

	private Jinjava jinjava;
	private Map<String, Object> jinjavaContext;
	private String pwd;

	public String getGroupId() {
		return Optional.ofNullable(this.groupId).orElse("org.jamgo");
	}

	public void setGroupId(final String groupId) {
		this.groupId = groupId;
	}

	public String getOrganizationName() {
		return Optional.ofNullable(this.organizationName)
			.orElse(Lists.reverse(Splitter.on('.').splitToList(this.getGroupId())).get(0));
	}

	public void setOrganizationName(final String organizationName) {
		this.organizationName = organizationName;
	}

	public String getAppName() {
		return Optional.ofNullable(this.appName).orElse("demo");
	}

	private String getAppNameLowerCase() {
		return this.getAppName().replaceAll("[\\s-_]", "").toLowerCase();
	}

	private String getAppNameCamelCase() {
		return CaseUtils.toCamelCase(this.getAppName(), true, '-', '_', ' ');
	}

	public void setAppName(final String appName) {
		this.appName = appName;
	}

	public String getThemeName() {
		return Optional.ofNullable(this.themeName).orElse(this.getAppName());
	}

	public void setThemeName(final String themeName) {
		this.themeName = themeName;
	}

	public String getAppDescription() {
		return Optional.ofNullable(this.appDescription).orElse(this.getAppName());
	}

	public void setAppDescription(final String appDescription) {
		this.appDescription = appDescription;
	}

	public String getDbName() {
		return Optional.ofNullable(this.dbName).orElse(this.getAppName());
	}

	public void setDbName(final String dbName) {
		this.dbName = dbName;
	}

	public String getDbSchema() {
		return Optional.ofNullable(this.dbSchema).orElse(this.getAppNameLowerCase());
	}

	public void setDbSchema(final String dbSchema) {
		this.dbSchema = dbSchema;
	}

	public String getDbUser() {
		return Optional.ofNullable(this.dbUser).orElse(this.getOrganizationName());
	}

	public void setDbUser(final String dbUser) {
		this.dbUser = dbUser;
	}

	public String getDbPassword() {
		return Optional.ofNullable(this.dbPassword).orElse(this.getOrganizationName());
	}

	public void setDbPassword(final String dbPassword) {
		this.dbPassword = dbPassword;
	}

	public String getAppPackage() {
		return Optional.ofNullable(this.appPackage).orElse(
			StringUtils.joinWith(".", this.getGroupId(), this.getAppNameLowerCase()));
	}

	public void setAppPackage(final String appPackage) {
		this.appPackage = appPackage;
	}

	public boolean isOverwrite() {
		return this.overwrite;
	}

	public void setOverwrite(final boolean overwrite) {
		this.overwrite = overwrite;
	}

	public boolean isGenerateBackofficeApp() {
		return this.generateBackofficeApp;
	}

	public void setGenerateBackofficeApp(final boolean generateBackofficeApp) {
		this.generateBackofficeApp = generateBackofficeApp;
	}

	public boolean isGenerateWebApiApp() {
		return this.generateWebApiApp;
	}

	public void setGenerateWebApiApp(final boolean generateWebApiApp) {
		this.generateWebApiApp = generateWebApiApp;
	}

	public String getFrameworkVersion() {
		return this.frameworkVersion;
	}

	public void setFrameworkVersion(final String frameworkVersion) {
		this.frameworkVersion = frameworkVersion;
	}

	public String getDockerRepository() {
		return Optional.ofNullable(this.dockerRepository).orElse(this.getOrganizationName());
	}

	public void setDockerRepository(final String dockerRepository) {
		this.dockerRepository = dockerRepository;
	}

	@TaskAction
	void performCreateAppSkeletonTask() throws ClassNotFoundException, IOException {
		this.initializeJinjavaContext();
		this.pwd = this.getProject().getProjectDir().getAbsolutePath();

		final Reflections reflections = new Reflections(
			(new ConfigurationBuilder())
				.setUrls(ClasspathHelper.forPackage(CreateAppSkeletonTask.TEMPLATES_PACKAGE))
				.setScanners(new ResourcesScanner()));

		final String tempDir = Files.createTempDir().getAbsolutePath();

		reflections.getResources(Pattern.compile(".*\\.*")).stream()
			.filter(each -> each.startsWith(CreateAppSkeletonTask.TEMPLATES_PATH_COMMON))
			.forEach(each -> this.processResource(each, CreateAppSkeletonTask.TEMPLATES_PATH_COMMON, tempDir, true));

		if (this.isGenerateWebApiApp()) {
			reflections.getResources(Pattern.compile(".*\\.*")).stream()
				.filter(each -> each.startsWith(CreateAppSkeletonTask.TEMPLATES_PATH_WEB_API))
				.forEach(each -> this.processResource(each, CreateAppSkeletonTask.TEMPLATES_PATH_WEB_API, tempDir, true));
		}

		if (this.isGenerateBackofficeApp()) {
			reflections.getResources(Pattern.compile(".*\\.*")).stream()
				.filter(each -> each.startsWith(CreateAppSkeletonTask.TEMPLATES_PATH_BACKOFFICE))
				.forEach(each -> this.processResource(each, CreateAppSkeletonTask.TEMPLATES_PATH_BACKOFFICE, tempDir, true));
		}

		if (this.isGenerateWebApiApp() && this.isGenerateBackofficeApp()) {
			reflections.getResources(Pattern.compile(".*\\.*")).stream()
				.filter(each -> each.startsWith(CreateAppSkeletonTask.TEMPLATES_PATH_FULL))
				.forEach(each -> this.processResource(each, CreateAppSkeletonTask.TEMPLATES_PATH_FULL, tempDir, true));
		}

		java.nio.file.Files.walk(Paths.get(tempDir))
			.filter(each -> java.nio.file.Files.isRegularFile(each))
			.forEach(each -> this.copyFile(each, tempDir, this.pwd, this.overwrite));
	}

	private void initializeJinjavaContext() {
		final Properties pluginProperties = new Properties();
		String pluginVersion = null;
		try {
			pluginProperties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("plugin.properties"));
			pluginVersion = pluginProperties.getProperty("version");
		} catch (final IOException e) {
			pluginVersion = "unknown";
		}

		this.jinjava = new Jinjava();
		this.jinjavaContext = new HashMap<>();
		this.jinjavaContext.put("group_id", this.getGroupId());
		this.jinjavaContext.put("organization_name", this.getOrganizationName());
		this.jinjavaContext.put("framework_plugin_version", pluginVersion);
		this.jinjavaContext.put("app_name", this.getAppName());
		this.jinjavaContext.put("app_name_lower_case", this.getAppNameLowerCase());
		this.jinjavaContext.put("app_name_camel_case", this.getAppNameCamelCase());
		this.jinjavaContext.put("theme_name", this.getThemeName());
		this.jinjavaContext.put("app_description", this.getAppDescription());
		this.jinjavaContext.put("db_name", this.getDbName());
		this.jinjavaContext.put("db_schema", this.getDbSchema());
		this.jinjavaContext.put("db_user", this.getDbUser());
		this.jinjavaContext.put("db_password", this.getDbPassword());
		this.jinjavaContext.put("framework_version", this.getFrameworkVersion());
		this.jinjavaContext.put("app_package", this.getAppPackage());
		this.jinjavaContext.put("app_package_path", this.getAppPackage().replace('.', '/'));
		this.jinjavaContext.put("docker_repository", this.getDockerRepository());
	}

	private void processResource(final String resourceName, final String pathPrefix, final String outputDir, final boolean overwrite) {
		if (resourceName.endsWith(CreateAppSkeletonTask.TEMPLATE_SUFFIX)) {
			this.renderTemplate(resourceName, pathPrefix, outputDir, overwrite);
		} else {
			this.copyResource(resourceName, pathPrefix, outputDir, overwrite);
		}
	}

	private void copyResource(final String resourceName, final String pathPrefix, final String outputDir, final boolean overwrite) {
		try {
			final ByteSource resource = Resources.asByteSource(Resources.getResource(resourceName));
			final String outputFileName = StringUtils.removeStart(resourceName, pathPrefix);
			final File outputFile = this.createFile(outputFileName, outputDir);
			if (!outputFile.exists() || overwrite) {
				resource.copyTo(Files.asByteSink(outputFile));
			}
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void renderTemplate(final String templateName, final String pathPrefix, final String outputDir, final boolean overwrite) {
		Charset templateCharset = Charsets.UTF_8;
		if (templateName.endsWith(CreateAppSkeletonTask.PROPERTIES_TEMPLATE_SUFFIX)) {
			templateCharset = Charsets.ISO_8859_1;
		}
		try {

			final String template = Resources.toString(Resources.getResource(templateName), templateCharset);
			final String outputFileName = StringUtils.removeStart(
				StringUtils.removeEnd(templateName, CreateAppSkeletonTask.TEMPLATE_SUFFIX), pathPrefix);
			final File outputFile = this.createFile(outputFileName, outputDir);

			// ... Starting from gradle > 7.1.1 is not possible to execute a task without a settings.gradle. So we
			//     need an empty one to execute the createSkeleton task and overwrite it with the template.

			final boolean overwriteSettings = outputFile.length() == 0L && outputFile.getName().equals("settings.gradle");

			this.logger.info(String.format("Output file: %s, length: %s", outputFile.getName(), String.valueOf(outputFile.length())));

			if (!outputFile.exists() || overwrite || overwriteSettings) {
				Files.asCharSink(outputFile, templateCharset).write(this.jinjava.render(template, this.jinjavaContext));
				if (outputFile.getName().endsWith(".sh")) {
					outputFile.setExecutable(true);
				}
			}
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private File createFile(final String fileName, final String outputDir) throws IOException {
		final String outputFileName = this.jinjava.render(Paths.get(outputDir, fileName).toString(), this.jinjavaContext);
		this.logger.info(outputFileName);
		final File outputFile = new File(outputFileName);
		Files.createParentDirs(outputFile);
		return outputFile;
	}

	private void copyFile(final Path filePath, final String sourceDir, final String targetDir, final boolean overwrite) {
		final File inputFile = filePath.toFile();
		try {
			final File outputFile = this.createFile(StringUtils.removeStart(filePath.toString(), sourceDir), targetDir);
			if (!outputFile.exists() || overwrite || outputFile.length() == 0L) {
				Files.copy(inputFile, outputFile);
			}
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
