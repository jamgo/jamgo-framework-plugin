plugins {
	id "org.springframework.boot" version "2.7.7"
}

description = "{{app_description}} web api application"

apply plugin: "war"

// ...	Creates a separate configuration for each bootRun<environment>.
//		Environments are defined in superproject.

environments.findAll { it.isLocal }.each { eachEnvironment ->
	def runTaskName = getTaskName('Run', eachEnvironment)
	configurations.create("${runTaskName}Conf")
	def debugTaskName = getTaskName('Debug', eachEnvironment)
	configurations.create("${debugTaskName}Conf")
}

repositories {
	mavenLocal()
	mavenCentral()
}

dependencies {
	implementation(project(":{{app_name}}-web-api"))
	implementation("org.springframework.boot:spring-boot-starter-jdbc")
	implementation("org.springframework.security:spring-security-web")
	implementation("p6spy:p6spy")

	compileOnly("com.h2database:h2")

	providedRuntime("org.postgresql:postgresql:42.2.8")
	providedRuntime("com.h2database:h2")
	providedRuntime("org.springframework.boot:spring-boot-starter-undertow")

	// ...	Adds resources according to each environment.
	environments.findAll { it.isLocal }.each { eachEnvironment ->
		def runTaskName = getTaskName('Run', eachEnvironment)
		"${runTaskName}Conf"(project(path: ":{{app_name}}-resources", configuration: "${eachEnvironment.name}Zip"))
		def debugTaskName = getTaskName('Debug', eachEnvironment)
		"${debugTaskName}Conf"(project(path: ":{{app_name}}-resources", configuration: "${eachEnvironment.name}Zip"))
	}

	testImplementation("org.jamgo:jamgo-test")
	testImplementation("org.jamgo:jamgo-model::tests")
	testImplementation(testFixtures(project(":{{app_name}}-model")))
}

bootJar {
	mainClass = "{{app_package}}.webapi.{{app_name_camel_case}}WebApiApplication"
	enabled = true
}

// ...	Creates one separated bootRun task for each environment. Ej: bootRunLocalH2, bootRunLocalPostgres.
// 		Each task is configured with the general classpath + configuration of corresponding environment.

environments.findAll { it.isLocal }.each { eachEnvironment ->
	def runTaskName = getTaskName('Run', eachEnvironment)

	task "$runTaskName"(type: org.springframework.boot.gradle.tasks.run.BootRun) {
		dependsOn ':{{app_name}}-resources:buildResources'
		dependsOn 'build'
		group = "Application"
		mainClass.set(bootJar.mainClass)

		doFirst() {
			classpath = configurations.getByName("${runTaskName}Conf") + sourceSets.main.runtimeClasspath
			jvmArgs += "-Dserver.port=8081"
		}
	}

	def debugTaskName = getTaskName('Debug', eachEnvironment)

	task "$debugTaskName"(type: org.springframework.boot.gradle.tasks.run.BootRun) {
		dependsOn ':{{app_name}}-resources:buildResources'
		dependsOn 'build'
		group = "Application"
		mainClass.set(bootJar.mainClass)

		doFirst() {
			classpath = configurations.getByName("${debugTaskName}Conf") + sourceSets.main.runtimeClasspath
			jvmArgs = [ "-XX:+UseG1GC", "-Xdebug", "-Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5005" ]
			jvmArgs += "-Dserver.port=8081"
		}
	}	
}

def getTaskName(type, environment) {
	return "boot" + "$type" + "$environment.name".split("-")*.capitalize().join()
}

publish {
	dependsOn bootJar
}

publishToMavenLocal {
	dependsOn bootJar
}

publishing {
	publications {
		mavenWar(MavenPublication) {
			artifact war
		}
	}
}
