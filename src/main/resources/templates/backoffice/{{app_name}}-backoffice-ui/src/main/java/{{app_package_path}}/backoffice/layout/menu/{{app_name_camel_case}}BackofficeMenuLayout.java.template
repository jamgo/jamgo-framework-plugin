package {{app_package}}.backoffice.layout.menu;

import org.jamgo.model.entity.AppRole;
import org.jamgo.model.entity.AppUser;
import org.jamgo.model.entity.Language;
import org.jamgo.model.entity.LocalizedMessage;
import org.jamgo.services.impl.LocalizedMessageService;
import org.jamgo.ui.layout.BackofficeApplicationDef;
import org.jamgo.ui.layout.details.SystemInfoLayout;
import org.jamgo.ui.layout.menu.MenuGroup;
import org.jamgo.ui.layout.menu.MenuItem;
import org.jamgo.ui.layout.menu.MenuLayout;
import org.jamgo.ui.security.layout.PermissionsLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.flow.component.icon.VaadinIcon;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class {{app_name_camel_case}}BackofficeMenuLayout implements MenuLayout {

	@Autowired
	protected BackofficeApplicationDef backofficeApplicationDef;
	@Autowired
	protected LocalizedMessageService messageSource;

	@Override
	public MenuGroup getRootMenuGroup() {
		final MenuGroup configurationLayoutDefsGroup = new MenuGroup();
		configurationLayoutDefsGroup.setNameSupplier(() -> this.messageSource.getMessage("menu.configuration"));
		configurationLayoutDefsGroup.setIcon(VaadinIcon.COG.create());
		configurationLayoutDefsGroup.addMenuItem(new MenuItem(this.backofficeApplicationDef.getLayoutDef(AppUser.class)));
		configurationLayoutDefsGroup.addMenuItem(new MenuItem(this.backofficeApplicationDef.getLayoutDef(AppRole.class)));
		configurationLayoutDefsGroup.addMenuItem(new MenuItem(this.backofficeApplicationDef.getLayoutDef(PermissionsLayout.class)));
		configurationLayoutDefsGroup.addMenuItem(new MenuItem(this.backofficeApplicationDef.getLayoutDef(Language.class)));
		configurationLayoutDefsGroup.addMenuItem(new MenuItem(this.backofficeApplicationDef.getLayoutDef(LocalizedMessage.class)));
		configurationLayoutDefsGroup.addMenuItem(new MenuItem(this.backofficeApplicationDef.getLayoutDef(SystemInfoLayout.class)));

		final MenuGroup menuGroup = new MenuGroup();
		menuGroup.setNameSupplier(() -> this.messageSource.getMessage("application.title"));
		menuGroup.addMenuItem(configurationLayoutDefsGroup);
		return menuGroup;
	}

}
