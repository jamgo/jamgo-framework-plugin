buildscript {
	repositories {
		maven { url "https://plugins.gradle.org/m2/" }
		maven { url "https://repo.spring.io/plugins-release" }
	}
	dependencies {
		classpath("io.spring.gradle:dependency-management-plugin:1.0.10.RELEASE")
		classpath('net.nemerosa:versioning:2.7.1')
		classpath("gradle.plugin.com.dorongold.plugins:task-tree:1.5")
	}
}

apply plugin: 'net.nemerosa.versioning'

versioning {
	releaseBuild = false
	releaseMode = { nextTag, lastTag, currentTag, extension -> "${currentTag}" }
	displayMode = 'snapshot'
	dirty = { t -> t }
}

configure(subprojects) { project ->
	group = "{{group_id}}"
	version = versioning.info.display

	ext.gradleScriptDir = "${rootProject.projectDir}/gradle"
	ext.environments = [
		[ name: "common", isLocal: false ],
		[ name: "local-h2", isLocal: true ],
		[ name: "local-postgres", isLocal: true ],
		[ name: "local-postgres-sc", isLocal: true ],
		[ name: "dev-postgres", isLocal: false ],
		[ name: "qa-postgres", isLocal: false ]
	]

	apply plugin: "maven-publish"
	apply plugin: "io.spring.dependency-management"
	apply plugin: "com.dorongold.task-tree"
	
	repositories {
		mavenLocal()
		mavenCentral()
		maven { url "https://vaadin.com/nexus/content/repositories/vaadin-addons/" }
		maven { url 'https://oss.sonatype.org/content/repositories/snapshots' }
	}

	dependencyManagement {
		imports {
			mavenBom "org.jamgo:jamgo-bom:{{framework_version}}"
			mavenBom "org.jamgo:jamgo-framework-bom:{{framework_version}}"
		}
	}
}

configure(subprojects) { project ->
	ext.nexusSnapshotsUrl = project.hasProperty("public_nexus_snapshots_url") ? "${public_nexus_snapshots_url}" : ""
	ext.nexusReleasesUrl = project.hasProperty("public_nexus_releases_url") ? "${public_nexus_releases_url}" : ""
	ext.nexusUrl = project.version.endsWith("-SNAPSHOT") ? "${nexusSnapshotsUrl}" : "${nexusReleasesUrl}"
	ext.nexusUsername = project.hasProperty("public_nexus_username") ? "${public_nexus_username}" : ""
	ext.nexusPassword = project.hasProperty("public_nexus_password") ? "${public_nexus_password}" : ""

	publishing {
		repositories {
			maven {
				credentials {
					username "${nexusUsername}"
					password "${nexusPassword}"
				}
				url "${nexusUrl}"
			}
		}
	}

}

configure(subprojects - project(":{{app_name}}-resources")) { project ->
	apply plugin: "java"

	sourceCompatibility = 11
	targetCompatibility = 11

	def commonCompilerArgs =
		["-Xlint:all", "-Xlint:-serial"]

	compileJava.options*.compilerArgs = commonCompilerArgs

	compileTestJava.options*.compilerArgs = commonCompilerArgs +
		["-Xlint:-varargs", "-Xlint:-deprecation"]

	compileJava {
		sourceCompatibility = 11
		targetCompatibility = 11
		options.encoding = 'UTF-8'
	}

	compileTestJava {
		sourceCompatibility = 11
		targetCompatibility = 11
		options.encoding = 'UTF-8'
		options.compilerArgs += "-parameters"
	}

	test {
		useJUnitPlatform()
		systemProperty("java.awt.headless", "true")
		scanForTestClasses = false
		include(["**/*Tests.class", "**/*Test.class"])
		// Since we set scanForTestClasses to false, we need to filter out inner
		// classes with the "$" pattern; otherwise, using -Dtest.single=MyTests to
		// run MyTests by itself will fail if MyTests contains any inner classes.
		exclude(["**/Abstract*.class", '**/*$*'])
		reports.junitXml.setDestination(file("$buildDir/test-results"))
	}

	task testsJar(type: Jar) {
		classifier 'test'
		from sourceSets.test.output
	}

	task sourcesJar(type: Jar) {
		classifier 'sources'
		from sourceSets.main.allJava
		from sourceSets.test.allJava
	}
	
	tasks.withType(PublishToMavenLocal) { it.dependsOn test }
	
	javadoc {
		failOnError false
    	source = sourceSets.main.allJava
    	classpath = configurations.compileClasspath
    }
    
	task javadocJar(type: Jar, dependsOn: javadoc) {
		classifier "javadoc"
		from javadoc.destinationDir
	}

    artifacts {
    	archives testsJar
    	archives sourcesJar
    }

}

/*
 * Workaround for java-test-fixtures issue (Gradle 7.5+)
 * Taken from : https://github.com/gradle/gradle/issues/21853#issuecomment-1259895474
 */
subprojects {
    apply plugin: 'eclipse'
    eclipse.classpath.file.whenMerged { classpath -> 
    	def testDependencies = classpath.entries.findAll { entry -> entry instanceof org.gradle.plugins.ide.eclipse.model.ProjectDependency && entry.entryAttributes.test }
        classpath.entries.removeAll testDependencies
    }
}

wrapper {
    gradleVersion = '7.6'
}
